# Environnement de Développement PHP 8.3.7

## Description du projet

### Introduction
Ce dépot va vous permettre de déployer un environnement de développement en PHP 8.3.7. Une fois démarré, vous pourrez créer un projet PHP

### Contenu
Dans ce container, vous trouverez des images de :
- [PHP 8.3.7](https://hub.docker.com/_/php).
- [MySql](https://hub.docker.com/_/mysql).
- [PhpMyAdmin](https://hub.docker.com/_/phpmyadmin).
- [MailDev](https://hub.docker.com/r/maildev/maildev).

## Prérequis
Pour utiliser ce container de manière simple, télécharger et lancer le logiciel [Docker Desktop](https://docs.docker.com/get-docker/)

Installer les extensions nécessaires dans VS Code :
- Docker
- Dev Containers

## Installation

### Installation du container
Une fois `Docker Desktop` installé, vous pouvez cloner ce dépot.
```
git clone https://gitlab.com/guillaume.nagiel/php_env.git
```

Puis ouvrez ce dossier avec `VSCode`.

Nous allons maintenant ouvrir cette image docker en tant que `Conteneur de développement` dans `VSCode`:
- Accédez à la palette de commandes en appuyant sur Ctrl+Shift+P (ou Cmd+Shift+P sur macOS).
- Saisissez `Dev Containers: Open Folder in Container`.
- S'il vous est demandé un fichier de configuration, selectionnez `PHP Dev Container`.

Le conteneur devrait s'installer avec les extentions `VSCode` pour `PHP`.

### Vérification des services lancés
Nous sommes maintenant positionné dans le dossier `/var/www` du container. 

Pour vérifier la version de `PHP`, et par la même occasion, voir si le serveur `Apache` est bien lancé, tapez
```
php -v
```
Vous devriez avoir la version 8.3.7 de `PHP`

Vous allez ensuite tester la version de `composer`
```
composer -V
```
Vous devriez avoir la version 2.7.6 de `composer`

Il ne reste plus qu'a tester le serveur `MySQL` et `PhpMyAdmin`. Pour cela, dans votre navigateur, rendez vous à l'adresse [http://localhost:8080/](http://localhost:8080/) et saisissez `root` pour l'utilisateur sans mot de passe. Si vous arrivez à vous connecter à `PhpMyAdmin`, c'est que le serveur `MySQL` est bien lancé.
Une autre solution consiste à lancer cette page directement depuis `Docker Desktop`. Dans le container `phpmyadmin_env`, cliquez sur `OPEN WITH BROWSER`.

### Création d'un projet PHP
Créer un dossier `project` dans cette environnement, puis créer un fichier `index.php` dans ce dossier

> :warning: **Si vous voulez changer le nom du projet ou installer plusieurs projets**: Il vous faudra modifier le fichier [vhosts.conf](/php/vhosts/vhosts.conf).


Contenu du fichier `index.php`
```php
<?php 
echo 'Hello World !!!',
?>
```
Le serveur étant déjà lancé, il ne vous reste plus qu'a aller à l'adresse [http://localhost:8741/](http://localhost:8741/) pour arriver sur votre magnifique `Hello World !!!`.

## Création de plusieurs projets PHP dans le dossier `project`

### Configuration du `vhost` et création d'un sous projet
Afin de gérer plusieurs projets dans notre dossier `project`, nous allons retoucher au fichier `/php/vhost/vhost.conf`
Voici un exemple de `vhost.conf` permettant de gérer un projet appelé `projet1`
```conf
<VirtualHost *:80>
    ServerName localhost

    DocumentRoot /var/www/project
    DirectoryIndex index.php

    <Directory /var/www/project>
        Options Indexes FollowSymLinks
        AllowOverride None
        Require all granted
    </Directory>

    # Alias for project projet1
    Alias /tp /var/www/project/projet1
    <Directory /var/www/project/projet1>
        Options Indexes FollowSymLinks
        AllowOverride None
        Require all granted
    </Directory>

    # Set environment variables
    SetEnv PROJECT_TP_PATH /tp
    SetEnv PROJECT_EXOSPDO_PATH /exospdo

    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
```

Nous avons ajouté un alias aux `VirtualHost` pointant vers un sous dossier.
Nous pouvons maintenant créer un sous dossier du même nom et un fichier `index.php`
```php
<?php 
echo 'Projet1';
?>
```
Allons vérifier à l'adresse [http://localhost:8741/projet1](http://localhost:8741/projet1)
> :warning: **cette opération sera à répeter à chaque nouveau projet**

### Modification du fichier `index.php` du dossier `project`
Afin de lister tous nos projets quand nous sommes à la racine de notre environement, nous allons modifier le ficher `/project/index.php`
```php
<?php
$projects = array_filter(glob('*'), 'is_dir');

echo "<h1>Liste des Projets</h1>";
echo "<ul>";
foreach ($projects as $project) {
    echo "<li><a href=\"/$project\">$project</a></li>";
}
echo "</ul>";
?>
```
A cette adresse [http://localhost:8741/](http://localhost:8741/), nous avons maintenant cette liste.

Bon développement :sunglasses:

### Arrêt des conteneurs
Pour stopper les conteneurs de cet environnement, saisissez
```
docker stop $(docker ps -q)
```

Ou stoppez les manuellement depuis `Docker Desktop`.
# Liste des fonctionnalités à ajouter

### Todo


### In Progress


### Done ✓

- [x] Créer le README.md  
- [x] Créer le TODO.md  